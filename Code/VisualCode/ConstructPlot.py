#!/usr/bin/env python3.8
"""
Construct a plot that shows the training results of a model.
The plot will show the loss over the number of trained epochs,
not the number of epochs that were provided.
"""

import numpy as np
import matplotlib.pyplot as plt

__author__ = "Rik van de Pol"
__version__ = "1.0.0"
__data__ = "16.06.2021"


class ConstructPlot:
    """
    Constructs a simple plot that shows how the loss of a model changes over a number of epochs.
    It accepts three arrays, one containing the training losses, one containing the validation losses and
    one containing the number of epochs the model trained. The epochs are plotted on the x-axis and both loss
    are plotted on he y-axis.
    """
    def __init__(self, loss, val_loss, epochs):
        self.loss = loss
        self.val_loss = val_loss
        self.epochs = epochs

    def plot_without_axis_break(self):
        """
        Create a visual representation of the training process of the model. The loss, validation loss
        and number of trained epochs are provided to the class and used to construct the plot.
        :return:
        """
        loss_array = np.array(self.loss)
        val_loss_array = np.array(self.val_loss)

        # Create a list of the number of epochs trained, not the number of epochs provided
        epochs = list(range(1, self.epochs + 1))

        # plot the loss values. X-axis is constructed based on the number of epochs trained, not number
        # of epochs provided
        plt.plot(epochs, loss_array)
        plt.plot(epochs, val_loss_array)

        # Set a low limit to the loss range
        plt.ylim(0, 0.05)

        plt.xlabel("Number of epochs")
        plt.ylabel("Loss")
        plt.legend(["MSE Training Loss", "MSE Validation Loss"])
        plt.savefig("LossOverTime.png")
        plt.show()

