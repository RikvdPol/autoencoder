#!/usr/bin/env python3.8
"""
Prepares the data for training the model.
This module scales the data to a value between 0 and 1.
After that, it splits the data into three parts: training, test and validation.
Finally, it creates a copy of the test data and transforms a percentage
of the samples of all genes to NaN, depending on the percentage that was provided.
"""

import argparse
import sys
import random
import copy
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn import preprocessing

__author__ = "Rik van de Pol"
__version__ = "1.0.0"
__data__ = "16.06.2021"


class DataProcessor:
    """
    Splits data into three parts. Makes a copy of one of those parts, and changes a percentage
    of rows per column dependent on the percentage that was provided.
    """
    def __init__(self, training_size, file):
        self.training_size = training_size/100
        self.file = file

    def separate_data(self):
        """
        Reads the provided file, perform scaling and separate the file intro three parts, based on the training size
        provided.
        :return: training, test, validation datasets
        """
        # Open the provided file
        data = pd.read_csv(self.file, sep="\t", header=0, index_col=0)
        print("File successfully read")
        # Shuffle the data
        data = data.sample(frac=1)

        # Collect the column names, after splitting they are added to the data
        colnames = list(data.columns.values)

        # Collect the rownames, after splitting they are added to the data
        rownames = list(data.index)

        # Initiate the scaling object and perform the transformation
        min_max_scaler = preprocessing.MinMaxScaler()
        scaled_data = min_max_scaler.fit_transform(data)

        # Changes the data back to a pandas dataframe from a numpy array that was created due to the scaling
        data = pd.DataFrame(scaled_data)

        # Set the column and rownames back. These are automatically removed when splitting the data.
        data.columns = colnames
        data.index = rownames

        # Randomly split the data into training, test and validation data
        training, test = train_test_split(data, train_size=self.training_size)
        print("Finished generating training data")

        # Split the remaining data in half
        test, validation = train_test_split(test, test_size=0.5)

        # Collect the column and rownames from the testdata, so that they are added after the inverse transform
        colnames = list(test.columns.values)
        rownames = list(test.index)

        # Changes the datatype back to a pandas dataframe, so that the column and row names can be changed
        # to what they originally were.
        test = pd.DataFrame(min_max_scaler.inverse_transform(test))

        # Rename the columns and rows
        test.columns = colnames
        test.index = rownames
        print("Finished generating test data")

        return training, test, validation

    @staticmethod
    def add_nans(file_dict, percentage_to_nan):
        """
        Creates a copy of the test dataset. Afterwards n% of the columns are randomly selected, with n being
        the percentage provided. Then, for each column, n% of the rows are randomly selected and transformed to np.nan.
        :param percentage_to_nan: The percentage of the data which should be changed to nan
        :param file_dict: Dictionary containing three training, validation and test data
        :return:file_dict: Dictionary containing four training, validation, test data and test data containing nans
                ncol: Number of columns in the provided file. In the case of the file used for training this corresponds
                to the genes.
        """
        ncol = file_dict["Test"].shape[1]
        nrow = file_dict["Test"].shape[0]
        file_dict.update({"Test_norm": copy.copy(file_dict["Test"])})

        print("Changing ", percentage_to_nan, "% to nan", sep="")
        # iterate through all columns in the dataset
        for col_index in range(0, ncol):
            # In those columns select a randomly select rows which are changed to NaN, up until a
            # certain percentage of all samples in a column are NaN
            for row_index in random.sample(range(0, nrow), round(nrow * (percentage_to_nan / 100))):
                # iloc filters through a pandas dataset by index
                # iloc[row_index, col_index]
                file_dict["Test_norm"].iloc[:, col_index].iloc[row_index, ] = np.nan

        print("Added nans to test copy")
        return file_dict, ncol


def main():
    # Define the parser arguments. These arguments are required for the program to function
    my_parser = argparse.ArgumentParser(description="Separate the data and train model")
    my_parser.add_argument('-f', help="File to be processed", required=True)
    my_parser.add_argument('-s', type=int, help="Percentage of file to be used as trainings data. The remaining"
                                                "data will be split evenly in test and validation data",
                           required=True)

    my_parser.add_argument("-n", type=int, help="Provide the percentage of data that needs to be transformed to nan in"
                                                "the test data")

    args = my_parser.parse_args()

    # Initiate the DataSeparator class and provide it with the training data size provided via the command line
    processor = DataProcessor(args.s, args.f)

    # Split the data into training, test and validation data by calling the separate_data method
    # from the custom DataSeparator class
    train_data, test_data, val_data = processor.separate_data()

    # Save the different parts in a dictionary for later access
    files = {"Training": train_data, "Test": test_data, "Validation": val_data}

    # Create a copy of test data and add nan to it
    files, ncol = processor.add_nans(file_dict=files, percentage_to_nan=args.n)
    print("Number of columns in data:\n", ncol)

    files["Training"].to_csv("Trainingdata.tsv", sep="\t")
    files["Validation"].to_csv("Validationdata.tsv", sep="\t")
    files["Test_norm"].to_csv("Testdata.tsv",  sep="\t")
    files["Test"].to_csv("TestdataNaN.tsv", sep="\t")

    return 0


if __name__ == '__main__':
    sys.exit(main())
