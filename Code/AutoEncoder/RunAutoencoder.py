#!/usr/bin/env python3.8
"""
This module functions to construct and train the autoencoder by using the AutoEncoder.py,
DataProcessor.py and ConstructPlot.py modules.
"""
import argparse
import sys
import time

import tensorflow as tf
import tensorflow_addons as tfa

import AutoEncoder
import DataProcessor
from Code.VisualCode import ConstructPlot

__author__ = "Rik van de Pol"
__version__ = "1.0.0"
__data__ = "16.06.2021"


def separate_data(training_size, file, percentage_to_nan):
    """
    :param training_size: The percentage of the provided data that will be used as training data
    :param file: File location
    :param percentage_to_nan: the percentage of the test data which will be transformed to nan
    :return: Dictionary containing training, validation, test and test containing nan data
             Number of columns in the provided file
    """
    # Initiate the DataSeparator class and provide it with the training data size provided via the command line
    processor = DataProcessor.DataProcessor(training_size, file)

    # Split the data into training, test and validation data by calling the separate_data method
    # from the custom DataSeparator class
    train_data, test_data, val_data = processor.separate_data()

    # Save the different parts in a dictionary for later access
    files_transposed = {"Training": train_data, "Test": test_data, "Validation": val_data}

    # Create a copy of test data and add nan to it
    files, ncol = processor.add_nans(file_dict=files_transposed, percentage_to_nan=percentage_to_nan)

    return files, ncol


def contruct_autoencoder(ncol):
    """
    Construct the autoencoder using the AutoEncoder class. The model is returned and later trained
    :param ncol: The number of columns in the provided file
    :return: Autoencoder Model
    """
    # Initiate autoencoder object
    automodel = AutoEncoder.AutoEncoder(ncol)

    # Create the sequentialal autoencoder object
    model = automodel.construct_autoencoder_sequential()
    return model


def mean_squared_error(y_pred, y_true):
    """
    Manually calculates the mean squared error loss.
    :param y_pred: Value predicted by the model
    :param y_true: The true value
    :return: Mean Squared Error Loss
    """
    return (y_pred - y_true) ** 2


def mean_absolute_error(y_pred, y_true):
    """
    Manually calculates the mean absolute error.
    :param y_pred: The true value
    :param y_true: The predicted value
    :return: Mean Absolute Error Loss
    """
    return abs(y_pred - y_true)


def root_mean_squared_error(y_true, y_pred):
    """
    Manually calculates the Root Mean Squared Error.
    :param y_true: The true value
    :param y_pred: The predicted value
    :return: Root Mean Squared Error Loss
    """
    return tf.keras.backend.sqrt(tf.keras.backend.mean(tf.keras.backend.square(y_pred - y_true)))


def train_model(automodel, files, epochs):
    """
    The model is trained by extracting the training dataset from the files dictionary. Furthermore the validation,
    test and test containing nans are extracted also. The validation data is used to calculate the validation loss which
    in turn is used to determine if either under or overfitting occurs. The two test data files and the model are than
    saved and later used in the ModelStatistics class.
    :param automodel: The untrained model
    :param files: The dictionary containing the files needed for training and testing
    :param epochs: Number of training instances
    :return: model
    """
    # Define the optimiser
    opt = tfa.optimizers.RectifiedAdam(learning_rate=0.0001, amsgrad=True, decay=0.005)

    # Construct early stopping. Training will stop early if 10% of the number of provided epochs have passed and
    # the model has not improved
    callback = tf.keras.callbacks.EarlyStopping(monitor='loss', restore_best_weights=True, patience=round(0.1 * epochs))

    # Compile the model
    automodel.compile(opt, loss="mean_squared_error")

    # Save the data present in the file dictionary into new variables
    x_train = files["Training"]
    x_val = files["Validation"]
    x_test = files["Test"]
    x_nan = files["Test_norm"]

    # Write the test file containing true data and test file containing nan data for later use and testing
    # x_nan.to_csv("../../Datasets/TrueData.tsv", sep="\t")
    # x_test.to_csv("../../Datasets/DataWithNaN.tsv", sep="\t")

    # Start a counter to know how long the model trained
    start = time.perf_counter()

    # Initiate model learning
    model = automodel.fit(x_train, x_train,
                          epochs=epochs,
                          batch_size=None,
                          shuffle=True,
                          use_multiprocessing=True,
                          callbacks=[callback],
                          validation_data=(x_val, x_val))

    print(automodel.summary())
    finish = time.perf_counter()
    print(f'Finished training in {round(finish - start, 2)} second(s)')
    # automodel.save("../../Models/TrainedModel")

    return model


def display_results(model):
    """
    Determines the number of epochs trained, the training loss and validation loss of the model.
    These are then passed to the class that will display the results in a plot.
    :param model: The newly trained model
    :return:
    """
    # Retrieve the loss of the model from the model history
    loss = model.history['loss']

    # Determines the number of epochs trained. Different from the number of epochs provided.
    epochs = len(loss)

    # Retrieve the validation  loss of the model from the model history
    val_loss = model.history['val_loss']

    plot = ConstructPlot.ConstructPlot(loss, val_loss, epochs)
    plot.plot_without_axis_break()


def main():
    tf.config.threading.set_intra_op_parallelism_threads(20)
    # Define the parser that accept a number between 1 and 100 that is to be used to determine the size of the training
    # data and accept a file
    my_parser = argparse.ArgumentParser(description="Separate the data and train model")

    # Define the file location
    my_parser.add_argument('-f', help="File to be processed", required=True)

    # Define the percentage of the file that will be used as training data
    my_parser.add_argument('-s', type=int, help="Percentage of file to be used as trainings data. The remaining"
                                                "data will be split evenly in test and validation data", required=True)

    # Define the number of epochs, or training instances
    my_parser.add_argument("--e", type=int, help="provide the number of epochs. If no value is provided"
                                                 "it will be set to 30", default=30)

    # Define the percentage of test data that needs to be changed to nan
    my_parser.add_argument("--n", type=int, help="Provide the percentage of data that needs to be transformed to nan in"
                                                 "the test data", default=0)
    args = my_parser.parse_args()

    # Checks if the provided training size is valid
    if args.s == 0 or args.s == 100:
        print("Incorrect training size provided. \n Shutting down")
        sys.exit(0)

    # Checks if the percentage of NaN is valid
    if args.n == 100:
        print("Program cannot function if all samples of a gene are NaN \n Shutting down")
        sys.exit(0)

    # Call the method that separates the data based on the provided arguments
    files, ncol = separate_data(args.s, args.f, args.n)

    # Construct the autoencoder model
    autoencoder = contruct_autoencoder(ncol)

    # Intiate the training of the autoencoder model
    model = train_model(autoencoder, files, args.e)

    # # Display the loss of the training and validation data
    display_results(model)

    return 0


if __name__ == '__main__':
    sys.exit(main())
