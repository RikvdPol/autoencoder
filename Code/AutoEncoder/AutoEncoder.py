#!/usr/bin/env python3.8
"""
This module construct the autoencoder.
It defines the number of neurons in each layer and the activation function in each layer.
A custom activation function that enables the model to handle NaN values is defines as well.
"""

import tensorflow as tf
import keras
import tensorflow_addons as tfa

__author__ = "Rik van de Pol"
__version__ = "1.0.0"
__data__ = "16.06.2021"


class AutoEncoder:
    """
    Construct an autoencoder, with the input and output layer being dependent on the number of
    columns in the input data.
    """
    def __init__(self, ncol):
        self.ncol = ncol

    def getx(self, inp):
        """
        Replaces all incoming genes by the average if that gene, ignoring the NaNs.
        :param inp:
        :return:
        """
        # tf.gather is a function for gathering number in a array with our desire rule
        # here we want to calc average of input without attention to the NANs
        return tf.reduce_mean(tf.gather(inp, tf.where(~tf.math.is_nan(inp))), axis=0)

    def input_activation(self, inp):
        """
        Calls the getx activation function and create new array
        containing the output of the getx function.
        :param inp:
        :return:
        """
        # tf.map_fn  :  define desire function f(x) for x input (lambda x : f(x))
        output = tf.map_fn(lambda x: tf.zeros_like(x) + self.getx(x), inp)
        return output

    def construct_autoencoder_sequential(self):
        """
        Construct the auto-encoder using the sequential tensorflow model.
        In each layer the number of neurons and a activation function is provided.
        In the sequential model a new layer can be added by using the add function.
        :return:
        """
        tf.keras.utils.get_custom_objects().update({"My_activation": self.input_activation})

        autoencoder = tf.keras.Sequential([tf.keras.layers.InputLayer(input_shape=(self.ncol,)),
                                           tf.keras.layers.Activation("My_activation"), ], name="InputLayer")

        # Encoder section
        autoencoder.add(keras.layers.Dense(2000, activation=tfa.activations.mish, use_bias=True,
                                           bias_initializer=tf.keras.initializers.Constant(0.1),
                                           kernel_initializer='he_normal', name="Encoder_HiddenLayer1"))

        autoencoder.add(tf.keras.layers.LayerNormalization())

        autoencoder.add(keras.layers.Dense(1000, activation=tfa.activations.mish, use_bias=True,
                                           bias_initializer=tf.keras.initializers.Constant(0.1),
                                           kernel_initializer='he_normal', name="Encoder_HiddenLayer2"))

        autoencoder.add(tf.keras.layers.LayerNormalization())

        # Latent space representation
        autoencoder.add(keras.layers.Dense(500, activation=tfa.activations.mish, use_bias=True,
                                           bias_initializer=tf.keras.initializers.Constant(0.1),
                                           kernel_initializer='he_normal', name="Encoder_HiddenLayer3"))

        autoencoder.add(tf.keras.layers.LayerNormalization())

        # Decoder section
        autoencoder.add(keras.layers.Dense(1000, activation=tfa.activations.mish, use_bias=True,
                                           bias_initializer=tf.keras.initializers.Constant(0.1),
                                           kernel_initializer='he_normal', name="Decoder_HiddenLayer1"))

        autoencoder.add(keras.layers.Dense(2000, activation=tfa.activations.mish, use_bias=True,
                                           bias_initializer=tf.keras.initializers.Constant(0.1),
                                           kernel_initializer='he_normal', name="Decoder_HiddenLayer2"))

        autoencoder.add(keras.layers.Dense(self.ncol, activation="linear", use_bias=True,
                                           bias_initializer=tf.keras.initializers.Constant(0.1),
                                           kernel_initializer='he_normal', name="OutputLayer"))

        return autoencoder
