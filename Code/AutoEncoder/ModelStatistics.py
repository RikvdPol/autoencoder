#!/usr/bin/env python3.8
"""
Performs several statistics to judge the performance of a trained autoencoder.
"""
import sys
from tensorflow import keras
import numpy as np
import tensorflow as tf
import tensorflow_probability as tfp
import pandas as pd
from sklearn import preprocessing
import argparse
# It seems like this import is not used, but evaluation is unable to finish without it because this import
# is used in the training of the model
import tensorflow_addons as tfa

__author__ = "Rik van de Pol"
__version__ = "1.0.0"
__data__ = "16.06.2021"


class ModelStatistics:
    def __init__(self, model):
        self.model = model

    @staticmethod
    def correlation(true_data, reconstructed_data):
        """
        Calculates the correlation matrix for both the true data and the reconstructed data
        :param true_data: Dataset containing the original test data without added nans
        :param reconstructed_data: Resulting dataset produced by the model when provided with test data containing nan
        :return: reconstructed_correlation, true_data_correlation
        """
        # Change the pandas dataframe to a numpy array so
        # correlation can be calculated
        true_data = np.asarray(true_data).astype("float32")
        reconstructed_data = np.asarray(reconstructed_data).astype("float32")

        # Calculate a correlation matrix for the predicted data and the true data.
        reconstructed_correlation = tfp.stats.correlation(reconstructed_data,
                                                          y=None, sample_axis=1,
                                                          event_axis=0,
                                                          keepdims=False,
                                                          name=None)

        true_data_correlation = tfp.stats.correlation(true_data,
                                                      y=None, sample_axis=1,
                                                      event_axis=0,
                                                      keepdims=False,
                                                      name=None)
        # print("True", true_data_correlation)

        return reconstructed_correlation, true_data_correlation

    @staticmethod
    def percentile(reconstructed_correlation, true_data_correlation):
        """
        Calculates the 95% percentile of the reconstructed correlation - true data correlation.
        :param reconstructed_correlation:
        :param true_data_correlation:
        :return:
        """
        percentile = tfp.stats.percentile(reconstructed_correlation - true_data_correlation, 95,
                                          axis=None, interpolation='linear', keepdims=False,
                                          validate_args=False,
                                          preserve_gradients=True, name=None)
        return percentile

    def make_reconstructed_feature_space(self, input_data):
        """
        Reconstruct data by providing the input data to the model
        :param input_data: Scaled test data with randomly added nan
        :return: reconstructed_data
        """
        # load the model and provide it with the data containing nan so that the data can be reconstructed
        input_data = np.asarray(input_data).astype("float32")
        with tf.device("/cpu:0"):
            loaded_model = keras.models.load_model(self.model)
            reconstructed_data = loaded_model(input_data, training=False)
            return reconstructed_data, loaded_model

    @staticmethod
    def scale_data(data_containing_nan):
        """
        Scale the data and return the scaler object, so that the reconstructed data can be descaled to
        regain the original input data
        :param data_containing_nan: Test data with randomly added nan
        :return: data_containing_nan_scaled, min_max_scaler
        """
        min_max_scaler = preprocessing.MinMaxScaler()
        data_containing_nan_scaled = min_max_scaler.fit_transform(data_containing_nan)
        print("Scaled Data")
        return data_containing_nan_scaled, min_max_scaler

    @staticmethod
    def descale_data(reconstructed_data, scaler):
        """
        Reverse the scaling that was performed by using the scaling object and the reconstructed data.
        The descaled data is now in the same format as the original data
        :param reconstructed_data: Numpy array of the descaled reconstructed data
        :param scaler: The scaler object initially used to scale the data, used here to regain the original input
        :return: descaled_data
        """
        descaled_data = scaler.inverse_transform(reconstructed_data)
        print("Descaled Data")
        np.savetxt("../../Datasets/DescaledDataTest.csv", descaled_data, delimiter="\t")
        return descaled_data


def main():
    my_parser = argparse.ArgumentParser(description="Accepts the data and model")

    # Define the file location
    my_parser.add_argument('-t', help="True data file", required=True)

    # Define the percentage of the file that will be used as training data
    my_parser.add_argument('-p', help="Test data file containing NaN values", required=True)

    # Define the number of epochs, or training instances
    my_parser.add_argument("-m", help="Trained Model", required=True)

    # Define the percentage of test data that needs to be changed to nan

    args = my_parser.parse_args()
    true_data = pd.read_csv(args.t, sep="\t")
    data_containing_nan = pd.read_csv(args.p, sep="\t")
    model = args.m

    # Intiate the ModelStatistics class
    stats = ModelStatistics(model)

    # Scale the data so that it is in the same format as what the model was trained for
    data_containing_nan_scaled, scaler = stats.scale_data(data_containing_nan)

    # Predict the data containing NaN values
    reconstructed_data, model = stats.make_reconstructed_feature_space(data_containing_nan_scaled)

    # Reverse the scaling, so that the original input data is regained
    descaled_data = stats.descale_data(reconstructed_data, scaler)

    # Calculate the correlation matrices of both the predicted and true data
    reconstructed_correlation, true_data_correlation = stats.correlation(true_data, descaled_data)

    # Calculate the 95% percentile of the predicted data correlation minus the true data correlation
    percentile = stats.percentile(reconstructed_correlation, true_data_correlation)

    # True data needs to be scaled for evaluation, because the model was trained with scaled data
    true_data2 = scaler.fit_transform(true_data)
    print(model.evaluate(true_data2, reconstructed_data))

    # # Print the percentile calculated in the percentile method
    print(percentile)

    return 0


if __name__ == '__main__':
    sys.exit(main())

