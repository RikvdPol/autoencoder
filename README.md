# README #
This readme will explain to everything that is necessary for the autoencoder to be build.
Please ensure that these instructions are followed before attempting to run the code, as it will likely 
not run properly. 

# Version #
Version 1.00.0

## Installations ##

### Languages and Interpreters
Download [python version 3.8.7](https://www.python.org/downloads/release/python-387/)
From this page select the 64-bit version and your corresponding operating system.


Next, download [Pycharm](https://www.jetbrains.com/pycharm/download/#section=windows)
Either the Professional or Community version can be used. This project used the
community edition.

Download R from [this](https://cran.r-project.org/) location.
Select your operating system, and the bit version you want to use.
The plots were created using the 64 bit version, but the 32 bit version is sufficient for this too.

### pip ###
pip is a package manager that can easily install packages in Python.
It is the main way in which new packages are installed during the project.


First download [get-pip.py](https://bootstrap.pypa.io/get-pip.py).
Then complete the installation by running the following command from the command line:
```bash
python get-pip.py
```
If the file is not found, double-check the installation folder and make sure the current location
contains the get-pip.py file.

This will complete installation. Check if installation was successful by running the 
following command:
```bash
python -m pip --version

```
If installation was successful you should see something similar to
```
pip 21.0.1 from C:\Users\Username\AppData\Local\Programs\Python\Python38\lib\site-packages\pip (python 3.8)
``` 

### Python packages ###
Many packages were used in the creation of the autoencoder. The following section will show how to easily 
install all these packages. Note that not all packages that were used need to be manually installed.
These packages are copy, random, sys and time. These are built into python, and are always accessible
for import.

The first program that needs to be installed is argparse. This package allows the program to accept
command line arguments that are required for the autoencoder to function properly. It is installed by running
the following command
```
pip install argparse
``` 

Pandas is a package that is capable of opening a file. It is stored in a pandas dataframe, which
allows for easy data manipulation. It is installed with the following command:
```
pip install pandas
``` 

Once arguments are provided via the commandline, and the input file is opened, the input data will be scaled and split into three parts.
This is achieved with the help of the sklearn package. The sklearn package is installed via the following command:
```
pip install sklearn
``` 

Then, TensorFlow is used to construct the base of the autoencoder. The TensorFlow sequential model is
used to build the autoencoder. TensorFlow is installed via the following command:
```
pip install tensorflow
``` 

The next package that is needed is keras. Keras is used to add new layers to the TensorFlow model.
It is installed the following command:
```
pip install keras
``` 

Finally, TensorFlow-Addons is required to gain access to the mish activation function, the main
activation funtion used in the model. It can be installed via the following command:
```
pip install tensorflow-addons
``` 

One final package is needed in order to properly evaluate the data, the package being numpy.
It is used to change the datatype of test data and predicted data so that the correlation matrix
of both datasets can be calculated. It is installed with the following command:
```
pip install numpy
``` 

Alternatively, all packages can be installed via PyCharm. When PyCharm is opened, select File --> Setting and
move to Python Interpreter. 

Clicking settings will open the following window:

![Settings](Images/PycharmSettings.png)


From this window click on the + sign that is circled, which opens up
the next window:

![Search for Packages](Images/SearchForPackages.png)

In the search bar simply type in the name and click on the install package button 
to install new packages.


### R libraries ###
First, download the [R language](https://cran.r-project.org/bin/windows/base/).
Select the version corresponding with your operating system and follow the download instructions.

Next, download the R interpreter, [RStudio](https://www.rstudio.com/products/rstudio/download/).
Fist, select the version you want to use and once again select your operating system in the next window.

With R and Rstudio downloaded, open RStudio and perform the following commands:

The only library needed to construct the plots is ggplot2. It is installed by running the 
following command 
```
install.packages(ggplot2)
```
from the console in R studio.

Then repeat the same command for the data.table package.
```
install.packages(data.table)
```

Change the values as explained by the comments to correctly display the results.
If you used different training sizes, or different percentages of NaN values added, change
these accordingly. The training sizes are changed on line 17 for the losses and line 20 for the
percentiles. The percentage of NaN are changed on line 18 and 21 for the losses and NaN values
respectively.

### CUDA ###
First things first, it is assumed that your graphics card is compatible with the CUDA toolkit.
Without a compatible graphics card, it will not be possible to run this program.

These steps will explain how to achieve this as clearly as possible, however it can be hard to have it working properly.
The online documentation if quite extensive, so if you run into trouble don't hesitate to simply search
for the error message, and it should be relatively easy to fix.

- Step 1 Download and Install Microsoft Visual Studio:  
  The first thing that should be done is to download and install the 
  [microsoft visual studio](https://visualstudio.microsoft.com/downloads/).
  This is required for the CUDA toolkit to function properly. Follow the installation steps and proceed to step 2.

- Step 2 Download and Install CUDE Toolkit:  
  Next the [NVIDIA CUDA Toolkit](https://developer.nvidia.com/cuda-downloads) should be downloaded.
  Select your operating system and follow the instructions. When the exe file is downloaded, follow the
  onscreen prompts to finish the installation.
  
- Step 3 Download and Install cuDNN:  
  The last step is to download and install [cuDNN](https://developer.nvidia.com/cudnn). In order to be able
  to download this you have to register to become a member of the NVIDIA Developer Program, but don't worry this is free.
  Once again follow the onscreen prompts and download the latest version. Unzip the downloaded file.
  

If you are unable to get this working properly, refer to [this](https://towardsdatascience.com/installing-tensorflow-with-cuda-cudnn-and-gpu-support-on-windows-10-60693e46e781) guide.
It will explain in detail how to download and install everything that is needed and greatly helped
me to get everything to work properly.

## Running the program ##

The program is run from the command line in the following way:
```
[Filepath to RunAutoencoder.py] -f Filename -s Training size in percentage --e [number of epochs] 
--n [Percentage of samples to be changed to nan per gene]
```

IMPORTANT NOTE:
Before being able to evaluate, a manual change has to be made to the created test file, and the test 
file containing NaN values. When opening these files remove the first tab in the upperleft, so that
the file immediately starts with a gene name. This is required in order to properly run the ModelStatistics.py file.

Then, the ModelStatistics file can be run in the following way:
```
[Filepath to ModelStatistics.py] -t [Path to file containing the true data] 
-p [Path to copy of data containing NaN values] -m [Path to trained model] 
--n [Percentage of samples to be changed to nan per gene]
```

If you have any question regarding this program, or if you run into issues that makes the program
unable to run, please don't hesitate to contact me at: r.van.de.pol@st.hanze.nl.
I will try to help you and get back to you as soon as I can.

### Licence ###
[MIT License](https://choosealicense.com/licenses/mit/)

Copyright © 2021 Rik van de Pol  
(Student Bioinformatics at the Hanze University Applied Sciences)  

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
